package com.sptpc.library.service;

import com.sptpc.library.model.User;
import com.sptpc.library.model.vo.UserVo;
import com.sptpc.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    // 增
    public User insert(String userName, String userPassword, String userTrueName, String sex, Long userType) {
        return userRepository.save(User.builder()
                .userName(userName)
                .userPassword(userPassword)
                .userTrueName(userTrueName)
                .sex(sex)
                .userType(userType)
                .build());
    }


    public String deleteByUserId(Long userId) {
//        userRepository.deleteByUserId(userId);
//        return null;
        Optional<User> user = userRepository.findAllByUserId(userId);
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return "删除成功";
        } else {
            return null;
        }
    }

    public String deleteByTrueName(String userTrueName) {
        Optional<User> user = userRepository.findAllByUserTrueName(userTrueName);
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return "删除成功";
        } else {
            return "找不到要删除的用户";
        }
    }

    // 查
    // 查询所有
    public List<User> findAll() {
        return userRepository.findAll();
    }
    // 根据id查询
    public User findByUserId(Long userId) {
        return userRepository.findByUserId(userId);
    }
    // 根据真实姓名查询
    public User findAllByTrueName(String userTrueName) {
        Optional<User> user = userRepository.findAllByUserTrueName(userTrueName);
        if (user.isPresent()) {
            return user.get();
        } else {
            return null;
        }
    }
    // 根据用户名查询
    public User findAllByName(String userName) {
        Optional<User> user = userRepository.findAllByUserName(userName);
        if(user.isPresent()) {
            return user.get();
        } else {
            return null;
        }
    }
    // 模糊查询 userTrueName
    public User findOneLike(String userTrueName) {
        Optional<User> user = userRepository.findByUserTrueNameLike(userTrueName);
        if (user.isPresent()) {
            return user.get();
        } else {
            return null;
        }
    }

    // 改
    public User update(UserVo userVo) {
        Optional<User> oldUser = userRepository.findById(userVo.getUserId());
        if (oldUser.isPresent()) {
            User user = oldUser.get();
            user.setUserId(userVo.getUserId());
            user.setUserName(userVo.getUserName());
            user.setUserPassword(userVo.getUserPassword());
            user.setUserTrueName(userVo.getUserTrueName());
            user.setSex(userVo.getSex());
            user.setUserType(userVo.getUserType());
            return userRepository.saveAndFlush(user);
        } else {
            return null;
        }
    }

    public User update(User userVo) {
        Optional<User> oldUser = userRepository.findById(userVo.getUserId());
        if (oldUser.isPresent()) {
            User user = oldUser.get();
            user.setUserId(userVo.getUserId());
            user.setUserName(userVo.getUserName());
            user.setUserPassword(userVo.getUserPassword());
            user.setUserTrueName(userVo.getUserTrueName());
            user.setSex(userVo.getSex());
            user.setUserType(userVo.getUserType());
            return userRepository.saveAndFlush(user);
        } else {
            return null;
        }
    }

    // 增
    private static User userVoToUser(UserVo userVo) {
        User user = new User();
        user.setUserId(userVo.getUserId());
        user.setUserName(userVo.getUserName());
        user.setUserPassword(userVo.getUserPassword());
        user.setUserTrueName(userVo.getUserTrueName());
        user.setSex(userVo.getSex());
        user.setUserType(userVo.getUserType());
        return user;
    }

    public User save(UserVo userVo) {
        User newUser = userVoToUser(userVo);
        newUser.setUserType((long) 1);
        return userRepository.saveAndFlush(newUser);
    }

    public User updatePersonal(UserVo userVo) {
        User user = userRepository.findByUserId(userVo.getUserId());
        user.setUserName(userVo.getUserName());
        user.setUserTrueName(userVo.getUserTrueName());
        user.setSex(userVo.getSex());
        userRepository.saveAndFlush(user);
        return user;
    }

    public User findOneByUserName(String userName) {
        return userRepository.findOneByUserName(userName);
    }

    public List<User> findAllByUserNameOrUserTrueName(String userName, String userTrueName) {
        return userRepository.findAllByUserNameOrUserTrueName(userName,userTrueName);
    }


    public Optional<User> findAllByUserName(String userName) {
        return userRepository.findAllByUserName(userName);
    }
}

