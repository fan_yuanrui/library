package com.sptpc.library.service;

import com.sptpc.library.model.Book;
import com.sptpc.library.model.Borrowing;
import com.sptpc.library.model.User;
import com.sptpc.library.model.vo.BookVo;
import com.sptpc.library.repository.BookRepository;
import com.sptpc.library.repository.BorrowingRepository;
import com.sptpc.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BorrowingRepository borrowingRepository;

    //增
    public Book insert(String bookName, String author,
                             String publish, String bookKind,
                             Long bookExisting,
                             Long bookLending, Long bookBorrowing) {
        return   bookRepository.save(Book.builder()
                .bookName(bookName)
                .author(author)
                .publish(publish)
                .bookKind(bookKind)
                .bookExisting(bookExisting)
                .bookLending(bookLending)
                .bookBorrowing(bookBorrowing)
                .build());
    }

    //删
    // 根据id
    public boolean deleteById(long id) {
        try {
            bookRepository.deleteById(id);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public String deleteByBookName(String bookName) {
        Optional<Book> book = bookRepository.findAllByBookName(bookName);
        if (book.isPresent()) {
            bookRepository.delete(book.get());
            return "删除成功";
        } else {
            return "找不到要删除的图书名称";
        }
    }

    //查
    //得到所有图书信息
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    //id查询
    public Book findId(Long id) {
        Optional<Book> book = bookRepository.findById(id);
        if (book.isPresent()) {
            return book.get();
        } else {
            return null;
        }
    }

    //根据图书名称查询图书信息
    public Book findBookName(String bookName) {
        Optional<Book> book = bookRepository.findAllByBookName(bookName);
        if (book.isPresent()) {
            return book.get();
        } else {
            return null;
        }
    }

    //根据作者查询图书信息
    public List<Book> findAllByAuthor(String author) {
        List<Book> book = bookRepository.findALLByAuthor(author);
        if (book.size() > 0) {
            return book;
        } else {
            return null;
        }
    }

    //根据图书类别查找
    public List<Book> findAllByBookKind(String bookKind) {
        List<Book> book = bookRepository.findALLByBookKind(bookKind);
        if (book.size() > 0) {
            return book;
        } else {
            return null;
        }
    }

    //根据出版社查找
    public List<Book> findAllByPublish(String publish) {
        List<Book> book = bookRepository.findALLByPublish(publish);
        if (book.size() > 0) {
            return book;
        } else {
            return null;
        }
    }

    // 模糊查询 bookName
    public List<Book> findByBookNameLike(String bookName) {
        List<Book> book = bookRepository.findByBookNameLike(bookName);
        if (book != null) {
            return book;
        } else {
            return null;
        }
    }

    // 模糊查询 author
    public List<Book> findByAuthorLike(String author) {
        List<Book> book = bookRepository.findByAuthorLike(author);
        if (book != null) {
            return book;
        } else {
            return null;
        }
    }

    // 模糊查询 bookKind
    public List<Book> findByBookKindLike(String bookKind) {
        List<Book> book = bookRepository.findByBookKindLike(bookKind);
        if (book != null) {
            return book;
        } else {
            return null;
        }
    }

//    2020-07-06
    // 根据bookName
//    public Book findByBookName(String bookName) {
//        return bookRepository.selectByBookName(bookName);
//    }
//    //根据author查询book
//    public Book findByAuthor(String author) {
//        return bookRepository.selectByAuthor(author);
//    }
//    //根据publish查询book
//    public Object findByPublish(String publish) {
//        return bookRepository.selectByPublish(publish);
//    }
//    //根据bookKind查询book
//    public Book findByBookKind(String bookKind) {
//        return bookRepository.selectByBookKind(bookKind);
//    }
//    // 查询所有
//    public List<Book> findAll() {
//        return bookRepository.selectAll();
//    }

    // 改
    public Book update(BookVo bookVo) {
        Optional<Book> oldBook = bookRepository.findById(bookVo.getBookId());
        if (oldBook.isPresent()) {
            Book book = oldBook.get();
            book.setBookName(bookVo.getBookName());
            book.setAuthor(bookVo.getAuthor());
            book.setPublish(bookVo.getPublish());
            book.setBookKind(bookVo.getBookKind());
            book.setBookExisting(bookVo.getBookExisting());
            book.setBookLending(bookVo.getBookLending());
            book.setBookBorrowing(bookVo.getBookBorrowing());
            return bookRepository.saveAndFlush(book);
        } else {
            return null;
        }
    }

    public Book findByBookId(Long bookId) {
        return bookRepository.findByBookId(bookId);
    }

    // 增
    private static Book bookVoToBook(BookVo bookVo) {
        Book book = new Book();
        book.setBookId(bookVo.getBookId());
        book.setBookName(bookVo.getBookName());
        book.setAuthor(bookVo.getAuthor());
        book.setPublish(bookVo.getPublish());
        book.setBookKind(bookVo.getBookKind());
        book.setBookExisting(bookVo.getBookExisting());
        book.setBookLending(bookVo.getBookLending());
        book.setBookBorrowing(bookVo.getBookBorrowing());
        return book;
    }
    public Book save(BookVo bookVo) {
        Book newBook = bookVoToBook(bookVo);
        return bookRepository.saveAndFlush(newBook);
    }

    public String deleteByBookId(Long bookId) {
        Optional<Book> book = bookRepository.findAllByBookId(bookId);
        if (book.isPresent()) {
            bookRepository.delete(book.get());
            return "删除成功";
        } else {
            return null;
        }
    }

//    public List<Book> select(BookVo bookVo) {
//        Book book = bookVoToBook(bookVo);
//        return bookRepository.select(book);
//    }
//
//    public Object findByBookNameOrByAuthorOrByPublishOrByBookKind(BookVo bookVo) {
//        Book book = bookVoToBook(bookVo);
//        return bookRepository.select(book);
//    }

    public List<Book> findAllByBookNameOrAuthorOrPublishOrBookKind(String bookName,String author,String  publish,String bookKind) {
        return bookRepository.findAllByBookNameOrAuthorOrPublishOrBookKind(bookName,author,publish,bookKind);
    }

    public void userLend(Long userId, Long bookId) {
        User user = userRepository.findByUserId(userId);
        Book book = bookRepository.findByBookId(bookId);
        Borrowing borrowing = new Borrowing();
        String str ="J"+System.currentTimeMillis();
        borrowing.setUserId(userId);
        borrowing.setUserName(user.getUserName());
        borrowing.setBookId(bookId);
        borrowing.setBookName(book.getBookName());
        borrowing.setBorrowDateTime(new Date());
        borrowing.setReturnDateTime(new Date());
        borrowing.setReturnBook("未归还");
        borrowingRepository.saveAndFlush(borrowing);
        return;
    }
}

