package com.sptpc.library.controller;

import com.sptpc.library.model.Borrowing;
import com.sptpc.library.model.User;
import com.sptpc.library.model.vo.BorrowingVo;
import com.sptpc.library.model.vo.UserVo;
import com.sptpc.library.repository.BorrowingRepository;
import com.sptpc.library.service.BookService;
import com.sptpc.library.service.BorrowingService;
import com.sptpc.library.service.UserService;
import com.sptpc.library.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.awt.*;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/borrowing")
public class BorrowingController {

    @Autowired
    private BorrowingService borrowingService;
    @Autowired
    private UserService userService;


    //查
    // 查询所有
    @GetMapping("/")
    @ResponseBody
    public ServerResponse list() {
        return  ServerResponse.createBySuccess(borrowingService.findAll());
    }

    // 修改
    @PostMapping(value = "/update", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public ServerResponse update(BorrowingVo borrowingVo) {
        return ServerResponse.createBySuccess(borrowingService.update(borrowingVo));
    }
    // 删除
    @GetMapping(value = "delete",params = "borrowId")
    @ResponseBody
    public ServerResponse delete(@RequestParam Integer borrowId) {
        Borrowing borrowing = borrowingService.findByBorrowId(borrowId.longValue());
        if (borrowing != null) {
            if (borrowingService.deleteByBorrowId(borrowId.longValue()) != null) {
                return ServerResponse.createBySuccessMessage("删除成功");
            } else {
                return ServerResponse.createByErrorMessage("删除不成功");
            }
        } else {
            return ServerResponse.createByErrorMessage("删除失败");
        }
    }

    // 视图
    // 借阅记录
    @GetMapping(value = "/borrowing_view")
    @ResponseBody
    public ModelAndView borrowing_view () {
        ModelAndView mv = new ModelAndView("admin/admin_borrowing");
        List<Borrowing> borrowingList = borrowingService.findAll();
        mv.addObject("borrowingList",borrowingList);
        return mv;
    }

    // 编辑状态
    @GetMapping(value = "/borrowing_edit", params = "borrowId")
    @ResponseBody
    public ModelAndView borrowing_edit (@RequestParam Long borrowId) {
        ModelAndView mv = new ModelAndView("admin/borrowing_edit");
        Borrowing borrowing = borrowingService.findByBorrowId(borrowId);
        mv.addObject("borrowing",borrowing);
        return mv;
    }

    // 用户查询
    // 弹窗
    @GetMapping("/borrowing_select_w")
    @ResponseBody
    public ModelAndView borrowing_select_view() {
        ModelAndView mv = new ModelAndView("admin/borrowing_select_w");
        return mv;
    }
    // 查询页面
    @GetMapping("/admin_select")
    @ResponseBody
    public ModelAndView borrowing_select(UserVo userVo) {
        ModelAndView mv = new ModelAndView("admin/admin_borrowing");
        Optional<User> userList = userService.findAllByUserName(userVo.getUserName());
        mv.addObject("userList",userList);
        return mv;
    }
}
