package com.sptpc.library.controller;

import com.sptpc.library.model.Borrowing;
import com.sptpc.library.model.User;
import com.sptpc.library.model.vo.UserVo;
import com.sptpc.library.service.BorrowingService;
import com.sptpc.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {

    @Autowired
    private UserService userService;
    @Autowired
    private BorrowingService borrowingService;

    @GetMapping("/")
    @ResponseBody
    public ModelAndView index () {
        ModelAndView modelAndView = new ModelAndView("/login");
        return modelAndView ;
    }

    @GetMapping("/register")
    @ResponseBody
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("/register");
        return modelAndView;
    }

    @GetMapping("/login")
    @ResponseBody
    public ModelAndView login () {
        ModelAndView modelAndView = new ModelAndView("/login");
        return modelAndView;
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public ModelAndView save(User user,HttpSession httpSession) {
        User oldUser = userService.findAllByName(user.getUserName());
        if (oldUser != null) {
            if (oldUser.getUserPassword().equals(user.getUserPassword())) {
                if (oldUser.getUserType() == 0) {
                    ModelAndView modelAndView = new ModelAndView("admin/admin_user");
                    List<User> userList = userService.findAll();
                    modelAndView.addObject("userList",userList);
                    httpSession.setAttribute("user",oldUser);
                    return modelAndView;
                } else {
                    ModelAndView modelAndView = new ModelAndView("user/personal");
                    List<Borrowing> borrowingList = borrowingService.findAll();
                    modelAndView.addObject("borrowingList",borrowingList);
                    httpSession.setAttribute("user",oldUser);
                    return modelAndView;
                }
            } else {
                ModelAndView modelAndView = new ModelAndView("/login");
                return modelAndView;
            }
        } else {
            ModelAndView modelAndView = new ModelAndView("/login");
            return modelAndView;
        }
    }

    @RequestMapping("/register")
    public ModelAndView register(UserVo userVo) {
        User oldUser = userService.findAllByName(userVo.getUserName());
        if (oldUser == null) {
            User newUser = userService.save(userVo);
            ModelAndView modelAndView = new ModelAndView("/login");
            return modelAndView;
        } else {
            return null;
        }
    }

    @GetMapping("/login_out")
    @ResponseBody
    public ModelAndView login_out(HttpSession httpSession) {
        ModelAndView mv = new ModelAndView("/");
        System.out.println(httpSession.getId());
        httpSession.removeAttribute( "user" );
        httpSession.invalidate();
        return mv;
    }
    
}
