package com.sptpc.library.controller;

import cn.hutool.json.JSONObject;
import com.sptpc.library.model.Book;
import com.sptpc.library.model.User;
import com.sptpc.library.model.vo.BookVo;
import com.sptpc.library.service.BookService;
import com.sptpc.library.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookService bookService;

    //增
    @PostMapping(value = "/insert", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public ServerResponse insert(@RequestParam(name = "bookName") String bookName,
                                 @RequestParam(name = "author") String author,
                                 @RequestParam(name = "publish") String publish,
                                 @RequestParam(name = "bookKind") String bookKind,
                                 @RequestParam(name = "bookExisting") Long bookExisting,
                                 @RequestParam(name = "bookLending") Long bookLending,
                                 @RequestParam(name = "bookBorrowing") Long bookBorrowing

    ) {
        return  ServerResponse.createBySuccess(bookService.insert(bookName,author,publish,bookKind,bookExisting
                ,bookLending,bookBorrowing));
    }

    //删除
    // 根据 bookId
//    @PostMapping(value = "/delete",params = "bookId")
//    @ResponseBody
//    public ServerResponse deleteById(@RequestParam (name = "bookId")Long bookId){
//        Book book=bookService.findId(bookId);
//        if(book!=null){
//            bookService.deleteById(bookId);
//            return ServerResponse.createBySuccessMessage("删除成功");
//        }
//        else {
//            return ServerResponse.createByErrorMessage("找不到要被删除的分类");
//        }
//    }
    // 根据 bookName
    @PostMapping(value = "/delete",params = "bookName")
    @ResponseBody
    public ServerResponse deleteByBookName(@RequestParam(name = "bookName") String bookName) {
        return ServerResponse.createBySuccess(bookService.deleteByBookName(bookName));
    }

    // 查
    // 查询所有
    @GetMapping("/")
    @ResponseBody
    public ServerResponse list() {
        return  ServerResponse.createBySuccess(bookService.findAll());
    }

    //通过图书名称得到所有Book信息
    @PostMapping(value = "/",params = "bookName")
    @ResponseBody
    public ServerResponse findBookName(@RequestParam String bookName){
        return ServerResponse.createBySuccess(bookService.findBookName(bookName));
    }
    //根据作者名字查找
    @PostMapping(value = "/",params = "author")
    @ResponseBody
    public ServerResponse findAllByAuthor(@RequestParam String author){
        return  ServerResponse.createBySuccess(bookService.findAllByAuthor(author));
    }
    //根据图书类别查找
    @PostMapping(value = "/",params = "bookKind")
    @ResponseBody
    public ServerResponse findAllByBookKind(@RequestParam String bookKind){
        return  ServerResponse.createBySuccess(bookService.findAllByBookKind(bookKind));
    }
    //根据出版社查找
    @PostMapping(value = "/",params = "publish")
    @ResponseBody
    public ServerResponse findAllByPublish(@RequestParam String publish) {
        return ServerResponse.createBySuccess(bookService.findAllByPublish(publish));
    }
    //通过图书名称得到所有Book信息
//    @PostMapping(value = "/",params = "bookName")
//    @ResponseBody
//    public ServerResponse findByBookName(@RequestParam String bookName){
//        return ServerResponse.createBySuccess(bookService.findByBookName(bookName));
//    }
//    //根据作者名字查找
//    @PostMapping(value = "/",params = "author")
//    @ResponseBody
//    public ServerResponse findByAuthor(@RequestParam String author){
//        return  ServerResponse.createBySuccess(bookService.findByAuthor(author));
//    }
//    //根据图书类别查找
//    @PostMapping(value = "/",params = "bookKind")
//    @ResponseBody
//    public ServerResponse findByBookKind(@RequestParam String bookKind){
//        return  ServerResponse.createBySuccess(bookService.findByBookKind(bookKind));
//    }
//    //根据出版社查找
//    @PostMapping(value = "/",params = "publish")
//    @ResponseBody
//    public ServerResponse findByPublish(@RequestParam String publish) {
//        return ServerResponse.createBySuccess(bookService.findByPublish(publish));
//    }
    // 模糊查询 bookName
    @PostMapping(value = "/findOneLike",params = "bookName")
    @ResponseBody
    public ServerResponse findBookNameLike(@RequestParam(name = "bookName") String bookName) {
        return ServerResponse.createBySuccess(bookService.findByBookNameLike(bookName));
    }
    //  模糊查询 author
    @PostMapping(value = "/findOneLike", params = "author")
    @ResponseBody
    public ServerResponse findByAuthorLike(@RequestParam String author) {
        return ServerResponse.createBySuccess(bookService.findByAuthorLike(author));
    }
    // 模糊查询 bookKind
    @PostMapping(value = "/findOneLike", params = "bookKind")
    @ResponseBody
    public ServerResponse findByBookKindLike(@RequestParam String bookKind) {
        return ServerResponse.createBySuccess(bookService.findByBookKindLike(bookKind));
    }



    // 改
    @PostMapping(value = "/update", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public ServerResponse update(BookVo bookVo){
        return ServerResponse.createBySuccess(bookService.update(bookVo));
    }

    //增
    @PostMapping(value = "add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public ServerResponse add(BookVo bookVo) {
        return ServerResponse.createBySuccess(bookService.save(bookVo));
    }

    // 删
    @GetMapping(value = "delete", params = "bookId")
    @ResponseBody
    public ServerResponse delete(@RequestParam Integer bookId) {
        Book book = bookService.findByBookId(bookId.longValue());
        if (book != null) {
            if (bookService.deleteByBookId(bookId.longValue()) != null) {
                return ServerResponse.createBySuccessMessage("删除成功");
            } else {
                return ServerResponse.createByErrorMessage("删除不成功");
            }
        } else {
            return ServerResponse.createByErrorMessage("删除失败");
        }
    }

    // 视图
    // 图书管理
    @GetMapping(value = "/book_view")
    @ResponseBody
    public ModelAndView book_view () {
        ModelAndView mv = new ModelAndView("admin/admin_book");
        List<Book> bookList = bookService.findAll();
        mv.addObject("bookList",bookList);
        return mv;
    }
    // 图书新增
    @GetMapping("/book_add")
    @ResponseBody
    public ModelAndView book_add() {
        ModelAndView mv = new ModelAndView("admin/book_add");
        List<Book> bookList = bookService.findAll();
        mv.addObject("bookList",bookList);
        return mv;
    }
    // 图书编辑
    @GetMapping(value = "/book_edit", params = "bookId")
    @ResponseBody
    public ModelAndView book_edit(@RequestParam Long bookId) {
        ModelAndView mv = new ModelAndView("admin/book_edit");
        Book book = bookService.findByBookId(bookId);
        mv.addObject("book",book);
        return mv;
    }
    // 管理员图书查询
    // 弹窗
    @GetMapping("/book_select_view")
    @ResponseBody
    public ModelAndView book_select_view() {
        ModelAndView mv = new ModelAndView("admin/book_select_w");
        return mv;
    }
    // 查询页面
    @GetMapping("/admin_select")
    @ResponseBody
    public ModelAndView admin_select(BookVo bookVo) {
        ModelAndView mv = new ModelAndView("admin/admin_book");
        List<Book> bookList =bookService.findAllByBookNameOrAuthorOrPublishOrBookKind(bookVo.getBookName(),bookVo.getAuthor(),bookVo.getPublish(),bookVo.getBookKind());
        mv.addObject("bookList",bookList);
        return mv;
    }

    @GetMapping(value = "/user_lend",params = "bookId")
    @ResponseBody
    public JSONObject user_lend(@RequestParam("bookId")Integer bookId , HttpSession session) {
        JSONObject resObj = new JSONObject();
        User user = (User)session.getAttribute("user");
        //添加借书订单
        bookService.userLend(user.getUserId(),bookId.longValue());
        //修改图书信息
//        bookService.updateLend(bookId);
        resObj.put("code", 0);
        resObj.put("msg", "借阅成功!请到借阅记录中查询。");
        return resObj;
    }

    // 管理员图书查询
    // 弹窗
    @GetMapping("/select")
    @ResponseBody
    public ModelAndView book_select_w() {
        ModelAndView mv = new ModelAndView("user/select");
        return mv;
    }
    // 查询页面
    @GetMapping("/user_select")
    @ResponseBody
    public ModelAndView user_select(BookVo bookVo) {
        ModelAndView mv = new ModelAndView("user/book");
        List<Book> bookList =bookService.findAllByBookNameOrAuthorOrPublishOrBookKind(bookVo.getBookName(),bookVo.getAuthor(),bookVo.getPublish(),bookVo.getBookKind());
        mv.addObject("bookList",bookList);
        return mv;
    }

}






