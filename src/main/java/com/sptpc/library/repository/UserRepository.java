package com.sptpc.library.repository;

import com.sptpc.library.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUserId (Long userId);

    Optional<User> deleteByUserId (Long userId);

    Optional<User> findAllByUserName(String userName);

    Optional<User> findAllByUserTrueName(String userTrueName);

    Optional<User> findAllByUserId(Long userId);

    Optional<User> findByUserTrueNameLike(String userTrueName);

    User findOneByUserName(String userName);

    List<User> findAllByUserNameOrUserTrueName(String userName, String userTrueName);

}
