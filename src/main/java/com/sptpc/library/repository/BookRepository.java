package com.sptpc.library.repository;

import com.sptpc.library.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {

    //根据书名查找
    Optional<Book> findAllByBookName(String bookName);

    //根据作者名字查找
    List<Book> findALLByAuthor(String author);

    //根据图书类别查找
    List<Book> findALLByBookKind(String bookKind);

    //根据出版社查找
    List<Book> findALLByPublish(String publish);

    // 根据图书姓名
    Book deleteByBookName(String bookName);

    // 根据书名 模糊查询
    List<Book> findByBookNameLike(String bookName);

    // 根据作者 模糊查询
    List<Book> findByAuthorLike(String author);

    // 根据种类 模糊查询
    List<Book> findByBookKindLike(String bookKind);

    Book findByBookId(Long bookId);

    Optional<Book> findAllByBookId(Long bookId);

//    Book selectByBookName(String bookName);
//
//    List<Book> selectAll();
//
//    Book selectByAuthor(String author);
//
//    Book selectByPublish(String publish);
//
//    Book selectByBookKind(String category);
//


    List<Book> findAllByBookNameOrAuthorOrPublishOrBookKind(String bookName,String author,String publish,String bookKind);

}

