package com.sptpc.library.repository;

import com.sptpc.library.model.Borrowing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BorrowingRepository extends JpaRepository<Borrowing, Long> {

    List<Borrowing> findByUserId(Long userId);

    Borrowing findByBorrowId(Long borrowId);

    Optional<Borrowing> findAllByBorrowId(Long borrowId);

    List<Borrowing> findAllByUserName(String userName);
}
