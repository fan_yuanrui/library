package com.sptpc.library.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

//@Data
//@AllArgsConstructor
public class BookVo {
    @NotNull
    private Long bookId;
    @NotEmpty
    private String bookName;
    @NotEmpty
    private String author;
    @NotEmpty
    private String publish;
    @NotEmpty
    private String bookKind;
    @NotNull
    private Long bookExisting;
    @NotNull
    private Long bookLending;
    @NotNull
    private Long bookBorrowing;

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getBookKind() {
        return bookKind;
    }

    public void setBookKind(String bookKind) {
        this.bookKind = bookKind;
    }

    public Long getBookExisting() {
        return bookExisting;
    }

    public void setBookExisting(Long bookExisting) {
        this.bookExisting = bookExisting;
    }

    public Long getBookLending() {
        return bookLending;
    }

    public void setBookLending(Long bookLending) {
        this.bookLending = bookLending;
    }

    public Long getBookBorrowing() {
        return bookBorrowing;
    }

    public void setBookBorrowing(Long bookBorrowing) {
        this.bookBorrowing = bookBorrowing;
    }
}
