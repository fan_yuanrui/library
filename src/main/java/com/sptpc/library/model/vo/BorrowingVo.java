package com.sptpc.library.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class BorrowingVo {
    @NotNull
    private Long borrowId;
    @NotNull
    private Long userId;
    @NotEmpty
    private String userName;
    @NotNull
    private Long bookId;
    @NotEmpty
    private String bookName;
    @NotEmpty
    private String returnBook;
}
