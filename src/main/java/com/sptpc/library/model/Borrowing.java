package com.sptpc.library.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "borrowing")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Borrowing implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long borrowId;
    private Long userId;
    private String userName;
    private Long bookId;
    private String bookName;
    @Column(updatable = false)
    @CreationTimestamp
    private Date borrowDateTime;
    @Column(updatable = false)
    @CreationTimestamp
    private Date returnDateTime;
    private String returnBook;
}

