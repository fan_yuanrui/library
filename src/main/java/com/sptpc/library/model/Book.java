package com.sptpc.library.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "book")
@Data
//@ToString(callSuper = true)
//@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;
    private String bookName;
    private String author;
    private String publish;
    private String bookKind;
    private Long bookExisting;
    private Long bookLending;
    private Long bookBorrowing;
}
