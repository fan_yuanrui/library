package com.sptpc.library;

import com.sptpc.library.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Slf4j
//@SpringBootApplication(exclude={DataSourceAutoConfiguration.class, HibernateValidatorConfiguration.class})
@SpringBootApplication
@EnableTransactionManagement
public class LibraryApplication implements ApplicationRunner {
    @Autowired
    private BookService bookService;

    public static void main(String[] args) {
        SpringApplication.run(LibraryApplication.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {

    }


}
